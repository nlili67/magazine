<div class="banner">

    <div class="container">

        <div class="row">
            <div class="col-lg-6">
                <h3>Southkhulshi,Road-1,lane-1,block-c,chittagong, Bangladesh </h3>
            </div>
            <div class="col-lg-6">

                <ul class="list-inline">
                    <li><a href="#home">info@ impact magazine.care</a>
                    </li>
                    <li class="footer-menu-divider">&sdot;</li>
                    <li><a href="#about">+880197637779</a>
                    </li>
                </ul>

            </div>
        </div>

    </div>
    <!-- /.container -->

</div>
<!-- /.banner -->

<!-- JavaScript -->
<script src="<?php bloginfo('template_url');?>/js/jquery-1.10.2.js"></script>
<script src="<?php bloginfo('template_url');?>/js/bootstrap.js"></script>
<script src="<?php bloginfo('template_url');?>/js/main.js"></script>

</body>

</html>

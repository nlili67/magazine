<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Magazine Care</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom Google Web Font -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>

    <!-- Add custom CSS here -->
    <link href="<?php bloginfo('template_url');?>/css/style.css" rel="stylesheet">

</head>

<body>

<nav class="navbar navbar-default navbar-fixed-top custom-navbar" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">

                <img class="img-responsive intro-image" src="<?php bloginfo('template_url');?>/img/main_logo.png" alt="">


            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-right navbar-ex1-collapse">

          <?php



          wp_nav_menu
          (

              array(


                  'theme_location'=>'mainmenu',

                  'menu_class'=> nav_nav_menu '


                  )



          );



          ?>




           <!-- <ul class="nav navbar-nav">
                <li><a href="#">About Us</a>
                </li>
                <li><a href="#">Work</a>
                </li>
                <li>
                    <a href="#myModal" role="button" data-toggle="modal">Contact Us</a>
                </li>
            </ul>-->
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>
</body>
</html>